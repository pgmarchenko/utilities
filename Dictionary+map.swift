//
//  Dictionary+map.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 11/15/15.
//  Copyright © 2015 Pavel Marchenko. All rights reserved.
//

import Foundation

/*
    Standard map returns array, this map returns dictionary
*/
extension Dictionary {
    init(_ elements: [Element]) {
        self.init()
        for (k, v) in elements {
            self[k] = v
        }
    }

    func map<U>(_ transformValue: (Value) -> U) -> [Key : U] {
        return Dictionary<Key, U>(self.map({ (key, value) in (key, transformValue(value)) }))
    }

    func map<T: Hashable, U>(_ transform: (Key, Value) -> (T, U)) -> [T : U] {
        return Dictionary<T, U>(self.map(transform))
    }
}
