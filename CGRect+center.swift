//
//  CGRect+center.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/22/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGRect {
    var rb: CGPoint {
        get {
            return CGPoint(x: self.origin.x + self.width,
                           y: self.origin.y + self.height)
        }
    }

    var center: CGPoint {
        return CGPoint(x: self.width / 2, y: self.height / 2)
    }
}
