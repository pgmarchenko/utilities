//
//  SystemFolders.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 1/7/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation

class SystemFolders {
    static var library: String {
        get {
            return SystemFolders.libraryUrl.path
        }
    }

    static var libraryUrl: URL {
        get {
            return FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        }
    }

    static var documents: String {
        get {
            return SystemFolders.documentsUrl.path
        }
    }

    static var documentsUrl: URL {
        get {
            return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        }
    }

    static var cache: String {
        return SystemFolders.cacheUrl.path
    }

    static var cacheUrl: URL {
        return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
    }

    class var freeDiskSpaceInBytes: Int64 {
        get {
            do {
                let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
                let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value
                return freeSpace!
            } catch {
                return 0
            }
        }
    }
}
