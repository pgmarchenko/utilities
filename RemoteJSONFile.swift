//
//  RemoteJSONFile.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 5/1/17.
//  Copyright © 2017 Pavel Marchenko. All rights reserved.
//

import Foundation
import SwiftyJSON

class RemoteJSONFile {
    
    typealias RemoteJSONFileUpdateHandler = (JSON) -> Bool
    
    init?(local: URL, remote: URL, updateHandler: @escaping RemoteJSONFileUpdateHandler) {

        file = RemoteFile(
            local: local,
            remote: remote,
            updateHandler: whenFileUpdated
        )
        
        handler = updateHandler
    }
    
    func checkOut() {
        file?.checkOut()
    }
    
    func getJSON() -> JSON? {
        return file?.getData()?.asJSON
    }
    
    fileprivate var handler: RemoteJSONFileUpdateHandler? = nil
    fileprivate var file: RemoteFile? = nil
}

extension RemoteJSONFile {
    fileprivate func whenFileUpdated(data: Data) -> Bool {
        var error: NSError?
        let json = JSON(data: data, options: .allowFragments, error: &error)
        
        if let e = error {
            debugPrint(e.description)
            return false
        }
        
        return handler?(json) ?? false
    }
}

extension Data {
    var asJSON: JSON? {
        var error: NSError?
        let json = JSON(data: self, options: .allowFragments, error: &error)
        
        if let e = error {
            debugPrint(e.description)
            return nil
        }
        
        return json
    }
}
