//
//  WeakTimerFactory.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 1/12/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation

struct WeakTimerFactory {
    class WeakTimer: NSObject {
        fileprivate var timer: Timer!
        fileprivate let callback: () -> Void

        fileprivate init(timeInterval: TimeInterval, userInfo: AnyObject?, repeats: Bool, callback: @escaping () -> Void) {
            self.callback = callback
            super.init()
            self.timer = Timer.scheduledTimer(timeInterval: timeInterval,
                                                                target: self,
                                                                selector: #selector(invokeCallback),
                                                                userInfo: userInfo,
                                                                repeats: repeats)
        }

        @objc
        fileprivate func invokeCallback() {
            callback()
        }
    }

    // Returns a new timer that has not yet executed, and is not scheduled for execution.
    static func scheduledTimerWithTimeInterval(_ timeInterval: TimeInterval,
                                               userInfo: AnyObject?,
                                               repeats: Bool,
                                               callback: @escaping () -> Void) -> Timer {
        return WeakTimer(timeInterval: timeInterval, userInfo: userInfo, repeats: repeats, callback: callback).timer
    }
}
