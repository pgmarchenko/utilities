//
//  NSTimeIntervael+PotisionalTime.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/22/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation

extension TimeInterval {
    struct DateComponents {
        static let formatterPositional: DateComponentsFormatter = {
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.minute, .second]
            formatter.unitsStyle = .positional
            formatter.zeroFormattingBehavior = .pad
            return formatter
        }()
    }
    var positionalTime: String {
        return DateComponents.formatterPositional.string(from: self) ?? ""
    }
}
