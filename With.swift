//
//  With.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 1/27/17.
//  Copyright © 2017 Pavel Marchenko. All rights reserved.
//

import Foundation

func with<T>(_ o: T, doAction: (T)->()) {
    doAction(o)
}
