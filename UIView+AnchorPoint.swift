//
//  UIView+AnchorPoint.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/22/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    /// Move anchorPoint, but leave the layer at the same place
    func anchorPointMove(_ p: CGPoint) {
        self.layer.anchorPoint = p
        self.layer.position = CGPoint(x:frame.width * p.x, y:self.frame.height * p.y)
    }

    func anchorPointReset() {
        self.anchorPointMove(CGPoint(x: 0.5, y: 0.5))
    }
}
