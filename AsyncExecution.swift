//
//  DelayBlockCall.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 3/23/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation

func delayOnMain(_ delay: Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func asyncOnBackground(_ closure: @escaping () -> Void) {
    DispatchQueue.global(qos: .default).async {
        closure()
    }
}

func asyncOnMainThread(_ closure: @escaping ()->()) {
    DispatchQueue.main.async {
        closure()
    }
}

func exclusive<T>(_ obj: AnyObject!, closure: () -> T) -> T {
    var syncResult = Int32(OBJC_SYNC_NOT_INITIALIZED)
    repeat {
        syncResult = objc_sync_enter(obj)
        if (syncResult != Int32(OBJC_SYNC_SUCCESS)) {
            debugPrint("... Failed to get exclusive access to object \(obj)")
        }

    } while syncResult != Int32(OBJC_SYNC_SUCCESS)

    defer { objc_sync_exit(obj) }

    return closure()
}
