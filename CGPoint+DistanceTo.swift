//
//  CGPoint+DistanceTo.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/22/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGPoint {
    func distanceTo(_ point: CGPoint) -> CGFloat {
        return sqrt(pow(self.x - point.x, 2) + pow(self.y - point.y, 2))
    }
}
