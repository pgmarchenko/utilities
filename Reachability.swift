//
//  Reachability.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 3/19/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit

open class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }
}

func showNoInternetWarning(_ vc: UIViewController) {
    let alert = UIAlertController(title: R.string.localizable.noInternetDialogTitle(),
                                  message: R.string.localizable.noInternetDialogMessage(),
                                  preferredStyle: .alert)

    alert.addAction(UIAlertAction(title: R.string.localizable.noInternetDialogOk(), style: .default, handler: nil))

    vc.present(alert, animated: true, completion: nil)
}
