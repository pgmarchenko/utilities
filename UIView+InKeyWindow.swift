//
//  UIView+CenterPoint.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/22/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func centerInKeyWindow() -> CGPoint {
        return UIApplication.shared.keyWindow!.convert(self.center, from: self.superview!)
    }

    func frameInKeyWindow() -> CGRect {
        return UIApplication.shared.keyWindow!.convert(self.frame, from: self.superview!)
    }
}
