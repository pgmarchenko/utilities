//
//  UIViewControllerContextTransitioning+helpers.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 2/6/17.
//  Copyright © 2017 Pavel Marchenko. All rights reserved.
//

import Foundation

extension UIViewControllerContextTransitioning {
    var toView: UIView? {
        return self.view(forKey: .to)
    }

    var fromView: UIView? {
        return self.view(forKey: .from)
    }
}
