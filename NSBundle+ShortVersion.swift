//
//  AppInfo.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/26/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation

extension Bundle {
    var shortVersion: String {
        // swiftlint:disable force_cast
        return self.infoDictionary!["CFBundleShortVersionString"] as! String
    }
}
