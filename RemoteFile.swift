//
//  RemoteFile.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 6/27/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import Alamofire

class RemoteFile {

    typealias RemoteFileUpdateHandler = (Data) -> Bool

    init?(local: URL, remote: URL, updateHandler: @escaping RemoteFileUpdateHandler) {
        localURL = local
        remoteURL = remote

        let fileName = localURL.lastPathComponent

        cachedURL = SystemFolders.libraryUrl.appendingPathComponent(fileName)

        etagKey = remoteURL.path

        self.updateHandler = updateHandler
    }

    convenience init?(localPath: String, remoteUrl: String, updateHandler: @escaping RemoteFileUpdateHandler) {
        guard let local = URL(string: localPath),
            let remote = URL(string: remoteUrl) else {

            return nil
        }
        self.init(local: local, remote: remote, updateHandler: updateHandler)
    }

    func getData() -> Data? {
        return (try? Data(contentsOf: cachedURL)) ?? (try? Data(contentsOf: localURL))
    }

    let localURL: URL
    let remoteURL: URL
    let cachedURL: URL

    fileprivate let updateHandler: RemoteFileUpdateHandler
    fileprivate let etagKey: String

    fileprivate var remoteConfigChechoutInProgress = false
}

extension RemoteFile {
    func checkOut() {
        let localEtag: String = UserDefaults.standard.string(forKey: etagKey) ?? "localTag"

        let headers = [
            "If-None-Match": localEtag
        ]

        self.remoteConfigChechoutInProgress = true
        Alamofire
            .request(remoteURL, headers: headers)
            .validate()
            .responseData { response in
                defer { self.remoteConfigChechoutInProgress = false }

                let HTTPStatusCodeNotModified = 304
                if response.response?.statusCode == HTTPStatusCodeNotModified {
                    debugPrint("RemoteFile.checkout: \(self.remoteURL.absoluteString) is not updated")
                    return
                }

                switch response.result {
                case .success(let value):
                    let etag = response.response?.allHeaderFields["Etag"] as? String ?? ""
                    
                    guard etag != localEtag else {
                        debugPrint("RemoteFile.checkout: \(self.remoteURL.absoluteString) is not updated")
                        return
                    }
                    
                    let data = NSData.init(data: value)
                    asyncOnMainThread {
                        if self.updateHandler(value) {
                            asyncOnBackground {
                                debugPrint("RemoteFile.checkout to file: \(self.cachedURL.path)")
                                data.write(toFile: self.cachedURL.path, atomically: true)
                            }
                            
                            if etag.characters.count > 0 {
                                UserDefaults.standard.setValue(etag, forKey: self.etagKey)
                                UserDefaults.standard.synchronize()
                            }
                        } else {
                            debugPrint("RemoteFile failed to parse!")
                        }
                    }
                case .failure(let error):
                    debugPrint("ERROR: RemoteFile.checkout: \(self.remoteURL.path) : \(error)")
                }
        }
    }
}
