//
//  UIButton+AddBorder.swift
//  DrumPadMachine
//
//  Created by Pavel Marchenko on 4/22/16.
//  Copyright © 2016 Pavel Marchenko. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func addBorder(_ width: CGFloat = 1, radius: CGFloat = 0) {
        self.addBorder(width, radius: radius, color: self.titleColor(for: UIControlState()))
    }
}

extension UIView {
    func addBorder(_ width: CGFloat = 1, radius: CGFloat = 0, color: UIColor?) {
        self.layer.borderWidth = width
        self.layer.borderColor = color?.cgColor
        self.layer.cornerRadius = radius
    }
}
