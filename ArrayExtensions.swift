//
// Created by Pavel Marchenko on 4/28/17.
// Copyright (c) 2017 Pavel Marchenko. All rights reserved.
//

import Foundation

extension Array {
    func updated(value: Element, at i: Index) -> Array {
        guard self.indices.contains(i) else { return self }
        
        return Array(self.prefix(upTo: i) + Array([value]) + self.suffix(from: i + 1))
    }
}
